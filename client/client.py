from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import grpc
from delivery_proto import delivery_pb2, delivery_pb2_grpc
import json


channel = grpc.insecure_channel('grpc_service:5000')
# channel = grpc.insecure_channel('localhost:50051')

stub = delivery_pb2_grpc.DeliveryControllerStub(channel)

class Delivery(BaseModel):
    id: str


app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event("startup")
async def init_grpc():
    channel = grpc.insecure_channel('grpc_service:5000')
    # channel = grpc.insecure_channel('localhost:50051')
    stub = delivery_pb2_grpc.DeliveryControllerStub(channel)

@app.post("/delivery/")
async def create_item(delivery: Delivery):
    response = stub.Create(delivery_pb2.Delivery(id_order=delivery.id))
    return {
        "message": response.id_order + " berhasil disimpan"
    }

@app.get("/delivery/{id}")
async def get_item(id: str):
    try:
        response = stub.Retrieve(delivery_pb2.DeliveryRetrieveRequest(id_order=id))
        print("Masuk try")
    except:
        response = stub.Create(delivery_pb2.Delivery(id_order=id))
        response = stub.Retrieve(delivery_pb2.DeliveryRetrieveRequest(id_order=id))
        print("Masuk except")
    # print(response)
    return {
        "id": response.id_order,
        "status": response.status
    }
