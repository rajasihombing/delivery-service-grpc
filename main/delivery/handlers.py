from delivery.views import DeliveryService
from delivery_proto import delivery_pb2_grpc


def grpc_handlers(server):
    delivery_pb2_grpc.add_DeliveryControllerServicer_to_server(DeliveryService.as_servicer(), server)