from django_grpc_framework import proto_serializers
from delivery.models import Delivery
from delivery_proto import delivery_pb2


class DeliveryProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Delivery
        proto_class = delivery_pb2.Delivery
        fields = ['id_order']

class DeliveryDetailsProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = Delivery
        proto_class = delivery_pb2.DeliveryDetails
        fields = ['id_order','status']