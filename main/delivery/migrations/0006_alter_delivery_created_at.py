# Generated by Django 4.0.5 on 2022-06-18 19:21

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delivery', '0005_alter_delivery_created_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 18, 19, 20, 59, 145127)),
        ),
    ]
