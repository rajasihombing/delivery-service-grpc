# Generated by Django 4.0.5 on 2022-06-17 18:30

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delivery', '0002_delivery_delete_post'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 18, 1, 30, 48, 758498)),
        ),
    ]
