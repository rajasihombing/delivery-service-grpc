from django.db import models
from datetime import datetime

status = (
    ("Pesanan dijemput", "Pesanan dijemput"),
    ("Pesanan dikirim", "Pesanan dikirim"),
    ("Pesanan tiba", "Pesanan tiba")
)

class Delivery(models.Model):
    id_order = models.CharField(max_length=100)
    status = models.CharField(
        max_length=255,
        choices=status,
        default="Pesanan dijemput"
    )
    created_at = models.DateTimeField(
        default=datetime.now()
    )
    delivered = models.BooleanField(
        default=False
    )

    class Meta:
        ordering = ['created_at']

