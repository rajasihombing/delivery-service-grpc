import grpc
from google.protobuf import empty_pb2
from django_grpc_framework.services import Service
from delivery.models import Delivery
from delivery.serializer import DeliveryProtoSerializer, DeliveryDetailsProtoSerializer


class DeliveryService(Service):
    def List(self, request, context):
        deliverys = Delivery.objects.all()
        serializer = DeliveryProtoSerializer(deliverys, many=True)
        for msg in serializer.message:
            yield msg

    def Create(self, request, context):
        serializer = DeliveryProtoSerializer(message=request)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.message

    def get_object(self, pk):
        try:
            return Delivery.objects.get(pk=pk)
        except Delivery.DoesNotExist:
            self.context.abort(grpc.StatusCode.NOT_FOUND, 'Delivery:%s not found!' % pk)

    def Retrieve(self, request, context):
        delivery = Delivery.objects.get(id_order=request.id_order)
        serializer = DeliveryDetailsProtoSerializer(delivery)
        print(serializer.message)
        return serializer.message

    def Update(self, request, context):
        delivery = self.get_object(request.id)
        serializer = DeliveryProtoSerializer(delivery, message=request)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return serializer.message

    def Destroy(self, request, context):
        delivery = self.get_object(request.id)
        delivery.delete()
        return empty_pb2.Empty()